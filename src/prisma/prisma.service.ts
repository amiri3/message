import {
  Injectable,
  Logger,
  OnModuleDestroy,
  OnModuleInit,
} from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { Model } from 'src/enum/model.enum';
import { encrypt } from 'src/helper/encryption.helper';
@Injectable()
export class PrismaService
  extends PrismaClient
  implements OnModuleInit, OnModuleDestroy
{
  private readonly logger = new Logger(PrismaService.name);

  async onModuleInit() {
    try {
      await this.$connect();

      // Add Message Body Save
      this.$use(async (params, next) => {
        const { action, args, dataPath, runInTransaction, model } = params;
        if (model === Model.MESSAEG && action === 'create') {
          args.data.body = encrypt(args.data.body, process.env.ENCRYPTION_KEY || "TEST");
        }
        return next(params);
      });
    } catch (error) {
      this.logger.error(error);
    }
  }

  async onModuleDestroy() {
    try {
      await this.$disconnect();
    } catch (error) {
      this.logger.error(error);
    }
  }
}
