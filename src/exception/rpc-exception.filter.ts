import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  BadRequestException,
  Logger,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';

@Catch(BadRequestException)
export class BadRequestExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(BadRequestExceptionFilter.name);
  catch(exception: BadRequestException, host: ArgumentsHost) {
    const ctx = host.switchToRpc();
    return {
      status: false,
      message: 'Bad Request',
      error: exception.message,
    };
  }
}
