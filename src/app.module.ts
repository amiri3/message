// BUILT IN MODULE
import { resolve } from 'path';

import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { MessageModule } from './module/message/message.module';
import { BadRequestExceptionFilter } from './exception/rpc-exception.filter';
import { APP_FILTER } from '@nestjs/core';
import { PrismaModule } from './prisma/prisma.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      envFilePath: resolve('.env'),
      validationSchema: Joi.object({
        DATABASE_URL: Joi.string().required(),
      }),
    }),
    PrismaModule,
    MessageModule,
  ],
  controllers: [],
  providers: [
    {
      useClass: BadRequestExceptionFilter,
      provide: APP_FILTER,
    },
  ],
})
export class AppModule {
  constructor() {}
}
