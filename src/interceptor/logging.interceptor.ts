import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const { getContext } = context.switchToRpc();
    const rpcContext = getContext()
    const now = Date.now();
    return next
      .handle()
      .pipe(
        tap(() =>
          console.log(
            `handle event : ${rpcContext.args[1]} => ${Date.now() - now}ms`,
          ),
        ),
      );
  }
}
