import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpStatus,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  timestamp: number;
  message: string | null;
  data: T;
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    //  {
    //     data: data?.list ? data.list : data,
    //     message: data?.message || null,
    //     timestamp: new Date().getTime(),
    //   }
    return next.handle().pipe(
      map((data) => ({
        data: data?.list ? data.list : data,
        message: data?.message || null,
        timestamp: new Date().getTime(),
      })),
    );
  }
}
