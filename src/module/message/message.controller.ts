import { Controller } from '@nestjs/common';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import {
  Ctx,
  MessagePattern,
  NatsContext,
  Payload,
} from '@nestjs/microservices';

@Controller()
export class MessageController {
  constructor(private readonly messageService: MessageService) {}
  @MessagePattern({ cmd: 'create_message' })
  async create(@Payload() createMessageDto: CreateMessageDto): Promise<object> {
    return await this.messageService.create(createMessageDto);
  }

  @MessagePattern({ cmd: 'my_message' })
  async listMyMessages(@Payload() userId: number): Promise<object[]> {
    return await this.messageService.listMyMessages(userId);
  }

  @MessagePattern({ cmd: 'message_for_me' })
  async listMessagesForMe(@Payload() userId: number): Promise<object[]> {
    return await this.messageService.listMessagesForMe(userId);
  }
}
