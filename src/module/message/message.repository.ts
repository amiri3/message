import { Logger } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { Message } from '@prisma/client';

export class MessageRepository {
  private readonly logger = new Logger(MessageRepository.name);
  constructor(private readonly prismaService: PrismaService) {
  }

  async createMessage(createMessageDto: CreateMessageDto): Promise<Message> {
    try {
      const { body, is_public, title, users_id_access, user_id } =
        createMessageDto;

      const createMessage = await this.prismaService.message.create({
        data: {
          body,
          title,
          user_id,
          is_public,
        },
      });
      if (!is_public && createMessage) {
        const createManyUserMessageAccessRecorde =
          await this.prismaService.userAccessMessage.createMany({
            data: users_id_access.map((userId) => {
              return {
                user_id: userId,
                message_id: createMessage.id,
              };
            }),
          });
      }
      return createMessage;
    } catch (error) {
      console.log(error);

      return null;
    }
  }
}
