import { Injectable } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Message } from '@prisma/client';
import { MessageRepository } from './message.repository';
import { PrismaService } from 'src/prisma/prisma.service';
import { decrypt } from 'src/helper/encryption.helper';

@Injectable()
export class MessageService {
  constructor(private readonly prismaService: PrismaService) {}
  async create(createMessageDto: CreateMessageDto): Promise<Message> {
    const { body, is_public, title, users_id_access, user_id } =
      createMessageDto;
    const createMessage = await this.prismaService.message.create({
      data: {
        body,
        title,
        user_id,
        is_public,
      },
    });
    if (!is_public && createMessage) {
      const createManyUserMessageAccessRecorde =
        await this.prismaService.userAccessMessage.createMany({
          data: users_id_access.map((userId) => {
            return {
              user_id: userId,
              message_id: createMessage.id,
            };
          }),
        });
    }
    return createMessage;
  }

  async listMyMessages(userId: number): Promise<object[]> {
    const encryptedMessage = await this.prismaService.message.findMany({
      where: {
        user_id: userId,
      },
    });
    const decryptedMessage = encryptedMessage.map((message) => {
      const { body } = message;
      const decryptBody = decrypt(body, process.env.ENCRYPTION_KEY || 'TEST');
      return { ...message, body: decryptBody };
    });
    return decryptedMessage;
  }
  async listMessagesForMe(userId: number): Promise<object[]> {
    const encryptedMessage =
      await this.prismaService.userAccessMessage.findMany({
        where: {
          user_id: userId,
        },
        select: {
          Message: true,
        },
      });
    const decryptedMessage = encryptedMessage.map((userAccessMessage) => {
      const { body } = userAccessMessage.Message;
      const decryptBody = decrypt(body, process.env.ENCRYPTION_KEY || 'TEST');
      return { ...userAccessMessage.Message, body: decryptBody };
    });
    return decryptedMessage;
  }
  findAll() {
    return `This action returns all message`;
  }

  findOne(id: number) {
    return `This action returns a #${id} message`;
  }

  update(id: number, updateMessageDto: UpdateMessageDto) {
    return `This action updates a #${id} message`;
  }

  remove(id: number) {
    return `This action removes a #${id} message`;
  }
}
